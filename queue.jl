using Plots
gr()

v=0.01
x=-2.0
t_r= 0.2
dt= 0.01
dmin=0.50

d() = v * t_r - 1.5 * v * abs(v) - dmin

a(x) = x > d() ? -1 : x < d() ? 1 : 0; ics = [x]
ips = [0]

buffer = [x for i in 0:Int64(t_r / dt)]

#=
for t in 0:1000
    global x, v
    v = v + a(pop!(buffer)) * dt
    x = x + v * dt
    push!(buffer, x)
    push!(ics, x)
    push!(ips, t)
end
=#

#plot(ics, ips)
plot(
    [cos(i) for i in 0:0.01:6.28],
    [sin(i) for i in 0:0.01:6.28],
    aspect_ratio=:equal, legend=:false
)
anim = @animate for t in 0:1000
    global x, v
    v = v + a(pop!(buffer)) * dt
    x = x + v * dt
    push!(buffer, x)
    scatter!([cos(x)], [sin(x)])
    scatter!([cos(0)], [sin(0)])
end every 20

gif(anim, "/Users/Marcello/Documents/Julia/aaa.gif", fps=60)
