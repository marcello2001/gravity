
#Impostazioni per la posizione iniziale
Imposta_x = 0.0
Imposta_y = -1.0

#Impostazioni per la velocità iniziale
Imposta_v_x = sqrt(2)
Imposta_v_y = 0

#Impostazioni per la simulazione:
#delta_t è il passo di integrazione.
delta_t = 0.0001
#t_max è la lunghezza di tempo in cui eseguire la simulazione.
t_max = 500



#x e y sono le coordinate del corpo.
#imposta la posizione alla posizione iniziale
x = Imposta_x
y = Imposta_y
#v_x e v_y sono le componenti della velocità del corpo
#imposta la velocità alla velocità iniziale
v_x = Imposta_v_x
v_y = Imposta_v_y
#Accertati di aver correttamente impostato posizione e velocità iniziali
#println("$x,$y $v_x, $v_y")

#le due funzioni di accelerazione gravitazionale del corpo;
#le espressioni sono ricavate direttamente dalla legge di Newton:
# a = Gm/d^2 * vettore(d)/d = vettore(r) Gm/d^3.
# Il centro attrattore è fisso in (0,0), con Gm = 1
a_x(x, y) = -x / (sqrt(x^2 + y^2))^3
a_y(x, y) = -y / (sqrt(x^2 + y^2))^3

#Definisci l'intervallo discreto di tempo in cui eseguire la simulazione
t = 0:delta_t:t_max

#Gli array in cui salvare le serie di punti che costituiscono la traiettoria
ics = [x]
ips = [y]

#Le due funzioni per aggiornare la velocità integrando l'accelerazione
accelera_x() = v_x + (a_x(x, y) * delta_t)
accelera_y() = v_y + (a_y(x, y) * delta_t)

#Le due funzioni per aggiornare la posizione integrando la velocità
muovi_x() = x + (v_x * delta_t)
muovi_y() = y + (v_y * delta_t)

#Questo ciclo esegue la simulazione
#costruisce cioè le serie di punti da mettere in grafico come traiettoria
for T in t
	#println("$x,$y")
    global x, y, v_x, v_y

    v_x = accelera_x()
    v_y = accelera_y()

    x = muovi_x()
    y = muovi_y()

    push!(ics, x)
    push!(ips, y)
end

plot(
    ics,
    ips,
    label = "Traiettoria con energia meccanica = 0: velocità iniziale = (sqrt(2),0), \nposizione iniziale = (0,-1)",
    legend = :topleft,
    title = "Interazione gravitazionale di un corpo con il punto (0,0),\n secondo la legge a=1/(x^2 + y^2)"
)
#scatter!(ics,ips, label="Traiettoria")

i = 0:0.1:20
plot!(i, i.^2 ./ 4 .- 1, label = "Parabola: fuoco = (0,0), vertice = (0,-1)")

scatter!(
    [0],
    [0],
    label = "Centro di attrazione gravitazionale (0,0)",
    markersize = 0.3
)



x=Imposta_x
y=Imposta_y

v_x=Imposta_v_x
v_y=Imposta_v_y

anim = @animate for i in 0:delta_t:t_max
	plot(ics, ips);
	global x, y, v_x, v_y
	v_x = accelera_x()
	v_y = accelera_y()

	x=muovi_x()
	y=muovi_y()
	scatter!([0],[0])
	scatter!([x],[y])
end every 500

gif(anim, "/Users/Marcello/Documents/Julia/GIF.gif", fps=60)
